import React, { useState } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";

const DetailedListItem = ({ item }) => {
  return (
    <View style={styles.listItemView}>
      <Text style={styles.listItemTitle}> {item.address} </Text>
      <Text style={styles.price}>
        {" "}
        {"£" + item.price_per_month + " per month"}
      </Text>
      <Image style={styles.image} source={{ uri: item.image }} />
      <Text style={styles.description}> {item.description} </Text>
      <Text style={styles.contactDetails}>
        {" "}
        Contact the seller at {item.contact_details}{" "}
      </Text>

      <TouchableOpacity style={styles.btn}>
        <Text style={styles.btnText}>Register Interest</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  listItemView: {
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-start",
  },
  listItemTitle: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#3a86ff",
    fontSize: 18,
    marginBottom: 5,
  },
  price: {
    fontSize: 16,
  },
  image: {
    width: "100%",
    height: undefined,
    aspectRatio: 1,
    marginTop: 15,
    marginBottom: 15,
  },
  description: {
    fontSize: 16,
    color: "black",
  },
  contactDetails: {
    fontSize: 15,
    color: "blue",
    marginTop: 30,
    alignSelf: "center",
  },
  btn: {
    backgroundColor: "#7dafff",
    borderRadius: 30,
    padding: 6,
    paddingHorizontal: 20,
    margin: 20,
    alignSelf: "center",
  },
  btnText: {
    fontSize: 20,
    color: "white",
    fontWeight: "400",
  },
});

export default DetailedListItem;
