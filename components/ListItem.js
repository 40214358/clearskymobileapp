import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { withOrientation } from "react-navigation";

const ListItem = ({ item, n }) => {
  return (
    <TouchableOpacity
      style={styles.listItem}
      onPress={() => {
        n.navigation.navigate("Listing", {
          item: item,
          naviation: n,
        });
      }}
    >
      <View style={styles.listItemView}>
        <Text style={styles.listItemTitle}> {item.address} </Text>
        <Text style={styles.listItemDescription}>
          {" "}
          {"£" + item.price_per_month + " per month"}{" "}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "white",
    padding: 16,
    marginTop: 10,
    marginHorizontal: 20,
    borderRadius: 8,
    elevation: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  listItemView: {
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-start",
  },
  listItemTitle: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#3a86ff",
  },
  listItemDescription: {
    alignItems: "center",
    color: "black",
  },
});

export default ListItem;
