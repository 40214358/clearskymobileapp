import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  homeBackground: {
    backgroundColor: "#7dafff",
    flex: 1,
  },
  btn: {
    color: "white",
  },
});

export { styles };
