import React, { useState } from "react";
import { StyleSheet, Text, View, Button, FlatList } from "react-native";
import ListItem from "./../../components/ListItem";
import { styles } from "./styles.js";

class Home extends React.Component {
  state = { items: null };
  //fetch property listings from API
  async componentDidMount() {
    try {
      const response = await fetch(`http://localhost:3001/listings`);
      const json = await response.json();
      this.setState({ items: json });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <View style={styles.homeBackground}>
        <FlatList
          data={this.state.items}
          renderItem={({ item }) => <ListItem item={item} n={this.props} />}
        />
      </View>
    );
  }
}

export default Home;
