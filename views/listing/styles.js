import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  listingBackground: {
    backgroundColor: "#7dafff",
    flex: 1,
  },
  listItemTitle: {
    fontSize: 18,
    fontWeight: "bold",
    color: "red",
    marginBottom: 20,
  },
  item: {
    backgroundColor: "white",
    padding: 16,
    marginTop: 10,
    marginHorizontal: 20,
    borderRadius: 8,
    elevation: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
});

export { styles };
