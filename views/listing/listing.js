import React from "react";
import { StyleSheet, View } from "react-native";
import DetailedListItem from "./../../components/DetailedListItem";
import { styles } from "./styles.js";

const Listing = (props) => {
  const item = props.item;
  return (
    <View style={styles.listingBackground}>
      <View style={styles.item}>
        <DetailedListItem item={item} />
      </View>
    </View>
  );
};

export default Listing;
