import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeView from "./views/home/home.js";
import ListingView from "./views/listing/listing.js";
import RegisterInterestView from "./views/registerInterest/registerInterest.js";

const Stack = createStackNavigator();

export default function AppContainer() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: "CLEARSKY REALTORS" }}
        />
        <Stack.Screen
          name="Listing"
          component={ListingScreen}
          options={{ title: "Listing" }}
        />
        <Stack.Screen
          name="RegisterInterest"
          component={RegisterInterestScreen}
          options={{ title: "RegisterInterest" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const HomeScreen = (props) => {
  return <HomeView navigation={props.navigation} />;
};

const ListingScreen = (props) => {
  return <ListingView item={props.route.params.item} />;
};

const RegisterInterestScreen = (props) => {
  return <RegisterInterestView item={props.route.params.item} />;
};
